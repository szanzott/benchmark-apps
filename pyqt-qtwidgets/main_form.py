# -*- coding: utf-8 -*-

from custom_model import ListOfObjects, DataSource
from main_ui import Ui_MainWindow

from PyQt5 import QtWidgets, QtCore, QtGui, QtChart


class MainForm(Ui_MainWindow):
    def __init__(self, main_window):
        super(MainForm, self)
        self.setupUi(main_window)

        # Instantiate the model
        self.list_of_objects = ListOfObjects()
        self.data_source = DataSource()

        # Wiring the model
        self.data_source.dataReceived.connect(self.update_series)

        # ## Setup the chart
        # Chart setup
        self.chart = QtChart.QChart()
        # self.chart.legend().hide()
        self.chart.setBackgroundBrush(QtGui.QBrush(QtCore.Qt.white))
        self.chart.setTitle("Chart of " + self.comboBox_1.currentText())

        # Axis setup
        self.xaxis = QtChart.QValueAxis()
        self.chart.setAxisX(self.xaxis)
        self.horizontalSlider.valueChanged.connect(self.xaxis.setMax)
        self.yaxis = QtChart.QValueAxis()
        self.chart.setAxisY(self.yaxis)

        # ChartView setup
        self.chartview = QtChart.QChartView(self.chart)
        self.chartview.setBackgroundBrush(QtGui.QBrush(QtCore.Qt.white))
        self.chartview.setRenderHint(QtGui.QPainter.Antialiasing)
        self.chartview.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        # Adding Chartview to the GUI
        self.chart_holder.addWidget(self.chartview)

        # ## Wire-up the GUI
        # ComboBox1 wiring
        self.comboBox_1.currentTextChanged.connect(self.set_combobox2_model)
        self.comboBox_1.currentTextChanged.connect(
            lambda: self.chart.setTitle("Chart of " + self.comboBox_1.currentText()))
        self.comboBox_1.currentTextChanged.connect(self.generate_series)

        # ComboBoxes model setup
        self.comboBox_1.setModel(self.list_of_objects)
        self.set_combobox2_model()

        # Start/Stop refreshing
        self.refreshButton.clicked.connect(self.toggle_chart_refresh)

        # Name-changing button
        self.pushButton_2.clicked.connect(lambda: self.pushButton_2.setText(self.lineEdit.text()))

        # ListView model setup
        self.listView.setModel(self.list_of_objects)
        self.listView.setAlternatingRowColors(True)
        self.listView.setStyleSheet("alternate-background-color: lightgray; background-color: white;");

        # ListView context menu
        self.listView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu);
        self.listView.customContextMenuRequested.connect(self.show_context_menu)

    def toggle_chart_refresh(self):
        self.data_source.update_series = not self.data_source.update_series
        if self.data_source.update_series:
            self.refreshButton.setText("Stop Refreshing")
        else:
            self.refreshButton.setText("Start Refreshing")

    def set_combobox2_model(self):
        """
            This operation is wrapped in a method in order to connect it to the relative signal
        """
        self.comboBox_2.clear()
        self.comboBox_2.addItems(self.list_of_objects.data(
                self.list_of_objects.index(self.comboBox_1.currentIndex(), 0),
                self.list_of_objects.PropertiesRole))

    def show_context_menu(self, position_point):
            """
                Context menus cannot be designed in QtDesigner easily
            """
            context_menu = QtWidgets.QMenu()

            action_cut = QtWidgets.QAction("Cut")
            action_cut.triggered.connect(lambda: print("Cut"))
            context_menu.addAction( action_cut)

            action_copy = QtWidgets.QAction("Copy")
            action_copy.triggered.connect(lambda: print("Copy"))
            context_menu.addAction(action_copy)

            action_paste = QtWidgets.QAction("Paste")
            action_paste.triggered.connect(lambda: print("Paste"))
            context_menu.addAction(action_paste)

            context_menu.addSeparator()
            submenu = QtWidgets.QMenu("Find/Replace")

            action_fn = QtWidgets.QAction("Find Next")
            action_fn.triggered.connect(lambda: print("Find Next"))
            submenu.addAction(action_fn)

            action_fp = QtWidgets.QAction("Find Previous")
            action_fp.triggered.connect(lambda: print("Find Previous"))
            submenu.addAction(action_fp)

            action_replace = QtWidgets.QAction("Replace")
            action_replace.triggered.connect(lambda: print("Replace"))
            submenu.addAction(action_replace)

            context_menu.addMenu(submenu)

            global_menu_position = self.listView.mapToGlobal(position_point)
            context_menu.exec(global_menu_position)

    def generate_series(self):
        self.chart.removeAllSeries()
        for series_index in range(len(self.comboBox_1.currentData(self.list_of_objects.PropertiesRole))):
            curve = QtChart.QLineSeries()
            curve.setName("Property {}".format(series_index))
            curve.setUseOpenGL(True)
            self.chart.addSeries(curve)
            curve.attachAxis(self.chart.axisX())
            curve.attachAxis(self.chart.axisY())

    def update_series(self):
        values_to_plot = self.data_source.data[0: int(self.horizontalSlider.value())]

        self.horizontalSlider.setMaximum(len(self.data_source.data))
        self.spinBox.setMaximum(len(self.data_source.data))
        offset = max(values_to_plot)/10
        self.yaxis.setMax(max(values_to_plot) + offset*len(self.chart.series()))

        # Monitoring labels
        self.label_3.setText("Data Source Statistics:\nBatch Size: {} points\nUpdates Frequency: {:.2f} Hz".format(\
            len(self.data_source.data), self.data_source.updates_frequency))

        for position, curve in enumerate(self.chart.series()):
            curve.blockSignals(True)  # Prevents the chart's repainting, speeding up significantly!
            curve.clear()
            for x, y in enumerate(values_to_plot):
                curve.append(x, y + offset * position)
            curve.blockSignals(False)  # Re-enables the signals, so the following signal triggers the repaint
            curve.pointsReplaced.emit()

