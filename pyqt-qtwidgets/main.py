# -*- coding: utf-8 -*-

import sys
from main_form import MainForm
from PyQt5 import QtWidgets


if __name__ == '__main__':

    # Instantiate the app and the window
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QMainWindow()

    # Load the GUI
    form = MainForm(main_window)

    # Start display
    main_window.show()
    sys.exit(app.exec_())




