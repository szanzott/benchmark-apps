/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package benchmark_app.models;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.security.auth.login.LoginException;

import cern.accsoft.security.rba.RBAToken;
import cern.accsoft.security.rba.login.LoginPolicy;
import cern.japc.AcquiredParameterValue;
import cern.japc.Parameter;
import cern.japc.ParameterException;
import cern.japc.Selector;
import cern.japc.SimpleParameterValue;
import cern.japc.SubscriptionHandle;
import cern.japc.factory.ParameterFactory;
import cern.japc.spi.SelectorImpl;
import cern.japc.support.SysoutParameterValueListener;
import cern.rba.util.lookup.RbaTokenLookup;
import cern.rba.util.relogin.RbaLoginService;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

public class DataSource {
    
    SubscriptionHandle sh;

    int updatesCount = 0;
    long startTime = Instant.now().getEpochSecond();
    
    ObservableList<Integer> data = FXCollections.observableArrayList();
    List<ListChangeListener<Integer>> dataListeners = new ArrayList<>();
    IntegerProperty dataLimit = new SimpleIntegerProperty();
    IntegerProperty dataLenght = new SimpleIntegerProperty(1);
    DoubleProperty dataFrequency = new SimpleDoubleProperty();
    
    
    public DataSource() {
        
        try {
            performLogin();
            Parameter p = ParameterFactory.newInstance().newParameter("LHC.BSRTS.5R4.B1/Image#imageSet");
            Selector s = new SelectorImpl("LHC.USER.ALL");
            
            //create a subscription handle
            sh = p.createSubscription(s, new SysoutParameterValueListener() {
                @Override
                public void valueReceived(String paramName, AcquiredParameterValue value) {
                   handleIncomingData(value);
                }
            });
            
            // Setup internal bindings
            this.data.addListener((ListChangeListener.Change<? extends Integer> c) -> {
                if(!data.isEmpty()) dataLenght.setValue(data.size());
            });
            
        } catch (ParameterException e) {
            e.printStackTrace();
        }
    }
    
    private void handleIncomingData(AcquiredParameterValue value) {
        int[] array = ((SimpleParameterValue)value.getValue()).getInts();
        List<Integer> list =Arrays.stream(array).boxed().collect(Collectors.toList()); 
        data.clear();
        data.addAll( list );
        
        this.updatesCount++;
        this.dataFrequency.setValue( updatesCount / (Instant.now().getEpochSecond() - this.startTime) );
        
        System.out.println(data.subList(0, 10) + "...");
    }
    
    public IntegerProperty dataLimitProperty() {
        return this.dataLimit;
    }
    
    public IntegerProperty dataLenghtProperty() {
        return this.dataLenght;
    }
    
    public DoubleProperty dataFrequencyProperty() {
        return this.dataFrequency;
    }
    
    public void registerSeries(XYChart.Series<Number, Number> series, int dataOffset) {
        
        ListChangeListener<Integer> listener = (ListChangeListener.Change<? extends Integer> c) -> {
            if(series != null) {
                Platform.runLater( () -> {
                    series.getData().clear();
                    
                    for(int x=0; x<this.dataLimit.getValue(); x++) {
                        series.getData().add(new XYChart.Data<Number, Number>(x, this.data.get(x)+dataOffset));
                    }
                });
            }
        };
        this.data.addListener(listener);
        this.dataListeners.add(listener);
    }
    
    public void removeAllListeners() {
        for (ListChangeListener<Integer> listener : this.dataListeners) {
            this.data.removeListener(listener);
        }
        this.dataListeners.clear();
    }
    
    public void start(){
        try {
            sh.startMonitoring();
        } catch (ParameterException e) {
            e.printStackTrace();
        }
    }
    
    public void stop() {
        sh.stopMonitoring();
    }
    
    public boolean isRunning() {
        return sh.isMonitoring();
    }
    
    /* 
     * RBA Login is required to access machine data
     */
    private void performLogin() {
        try {
            RbaLoginService service = new RbaLoginService();
            service.setLoginPolicy(LoginPolicy.LOCATION);
            service.setApplicationName("BenchmarkApp");
            service.setAutoRefresh(true);
            service.startAndLogin();
            RBAToken token = RbaTokenLookup.findClientTierRbaToken();
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }

}
