/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package benchmark_app.models;

import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ComplexObject {
    
    StringProperty name = new SimpleStringProperty("Unnamed Complex Object");
    StringProperty chartName = new SimpleStringProperty("Unnamed Chart");
    ObservableList<String> properties = FXCollections.observableArrayList();
    
    public ComplexObject(String objectName, int props) {
        if(objectName != null) {
            this.name.setValue(objectName);
            this.chartName.setValue("Chart of " + this.name.getValue());
        }
        for(int i=0; i<props; i++) {
            this.properties.add( this.name.getValue()+"_property_"+i );
        }
    }
    
    @Override
    public String toString() {
        return this.name.getValue();
    }

    public String getName() {
        return name.getValue();
    }

    public void setName(String name) {
        this.name.setValue(name);
    }
    
    public StringProperty nameProperty() {
        return this.name;
    }

    public String getChartName() {
        return chartName.getValue();
    }

    public void setChartName(String chartName) {
        this.chartName.setValue(chartName);
    }
    
    public StringProperty chartNameProperty() {
        return this.chartName;
    }

    public ObservableList<String> getProperties() {
        return FXCollections.observableArrayList(properties);
    }

    public void setProperties(List<String> properties) {
        this.properties = FXCollections.observableArrayList(properties);
    }
}
