
package benchmark_app.views;

import benchmark_app.models.ComplexObject;
import benchmark_app.models.DataSource;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.ContextMenuEvent;
import javafx.util.converter.NumberStringConverter;

/**
 * The controller for the root layout. The root layout provides the basic application layout containing a menu bar and
 * space where other JavaFX elements can be placed.
 */
public class RootLayoutController {
    
    DataSource dataSource = new DataSource();
    ObservableList<ComplexObject> listOfObjects = FXCollections.observableArrayList();
    ObjectProperty<ComplexObject> selectedObject = new SimpleObjectProperty<>(); // Shortcut for the selected object (see initialize())
    
    @FXML
    ComboBox<ComplexObject> parentCombo;
    
    @FXML
    ComboBox<String> propsCombo;
    
    @FXML
    LineChart<Number, Number> chart;
    
    @FXML
    NumberAxis xAxis;

    @FXML
    NumberAxis yAxis;
    
    @FXML
    Label batchSize;
    
    @FXML
    Label updatesFrequency;
    
    @FXML
    Button refreshButton;

    @FXML
    Slider slider;
    
    @FXML
    TextField sliderSetter;
    
    @FXML
    Button nameChangerBtn;
    
    @FXML
    CheckBox buttonEnablerChk;
    
    @FXML
    TextField newNameTxt;
    
    @FXML
    ListView<ComplexObject> list;
    
    /**
     * This method is executed as soon as the FXML is parsed. It provides all the bindings and the additional features
     * that didn't fit into the FXML definition (like context menus)
     */
    public void initialize() {
        
        // Populate the listOfObject with ComplexObjects
        for(int i=1; i<5; i++) {
            ComplexObject co = new ComplexObject("Parent_Object_"+i, i);
            listOfObjects.add( co );
        }
        this.selectedObject.setValue(this.listOfObjects.get(0));
        
        // ComboBoxes wiring
        this.parentCombo.setItems(this.listOfObjects);
        this.parentCombo.getSelectionModel().selectedItemProperty().addListener( 
            (ObservableValue<? extends ComplexObject> observable, ComplexObject oldValue, ComplexObject newValue) -> {
                    propsCombo.setItems(newValue.getProperties());
                    propsCombo.getSelectionModel().selectFirst();
                    selectedObject.setValue(newValue);;
            });
        
        // Slider and slideSetter setup
        this.sliderSetter.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, 
                String newValue) -> {
                 // Makes slideSetter numeric
                if (!newValue.matches("\\d*")) {
                    sliderSetter.setText(newValue.replaceAll("[^\\d]", ""));
                }
            });

        Bindings.bindBidirectional(this.sliderSetter.textProperty(), this.slider.valueProperty(), new NumberStringConverter()); 
        this.dataSource.dataLimitProperty().bind( this.slider.valueProperty() );
        this.slider.maxProperty().bind( this.dataSource.dataLenghtProperty() );
        
        // Monitoring labels
        this.dataSource.dataLenghtProperty().addListener( (ObservableValue<? extends Number> observable, Number oldValue, 
                Number newValue) -> batchSize.setText( "Batch Size: "+newValue + " points" ));
        this.dataSource.dataFrequencyProperty().addListener( (ObservableValue<? extends Number> observable, Number oldValue, 
                Number newValue) -> updatesFrequency.setText( "Updates Frequency: "+newValue+ " Hz" ));
        
        // Setup the Chart
        setupChart();
        
        // Populate the ListView
        this.list.setItems(this.listOfObjects);
        
        // Create the ContextMenu
        ContextMenu contextMenu = new ContextMenu();
        MenuItem item1 = new MenuItem("Cut");
        item1.setOnAction((ActionEvent event) -> handleDisplayAlert("cut a") );
        MenuItem item2 = new MenuItem("Copy");
        item2.setOnAction((ActionEvent event) -> handleDisplayAlert("copy a") );
        MenuItem item3 = new MenuItem("Paste");
        item3.setOnAction((ActionEvent event) -> handleDisplayAlert("paste a") );
        
        // Create the submenu
        Menu findmenu = new Menu("Find/Replace");
        MenuItem childMenuItem1 = new MenuItem("Find Next");
        childMenuItem1.setOnAction((ActionEvent event) -> handleDisplayAlert("find the next") );
        MenuItem childMenuItem2 = new MenuItem("Find Previous");
        childMenuItem1.setOnAction((ActionEvent event) -> handleDisplayAlert("find the previous") );
        MenuItem childMenuItem3 = new MenuItem("Replace");
        childMenuItem1.setOnAction((ActionEvent event) -> handleDisplayAlert("replace an") );
        
        // Build up the contextmenu
        findmenu.getItems().addAll(childMenuItem1, childMenuItem2, childMenuItem3);
        contextMenu.getItems().addAll(item1, item2, item3, findmenu);
 
        // Link contextmenu to the list
        this.list.setOnContextMenuRequested( (ContextMenuEvent event) -> 
            contextMenu.show(list, event.getScreenX(), event.getScreenY()) );
        
        // Triggers the bindings
        this.parentCombo.getSelectionModel().selectFirst();
        
        // Start receiving data
        this.dataSource.start();
        
    }
    
    private void handleDisplayAlert(String msg) {
        Alert alert = new Alert(AlertType.INFORMATION, "You just tried to " + msg + " word!", ButtonType.OK);
        alert.showAndWait();
    }
    
    private void setupChart() {
        
        // Remove the big dots
        this.chart.setCreateSymbols(false);
        this.chart.setAnimated(false);
        
        // React to selection changes
        this.selectedObject.addListener(
            (ObservableValue<? extends ComplexObject> observable, ComplexObject oldValue, ComplexObject newValue) -> {
                
                // Cleanup
                this.dataSource.removeAllListeners();
                this.chart.getData().clear();
                
                // Set chart title
                this.chart.setTitle(newValue.getChartName());
                  
                int index = 0;
                for(String prop : this.selectedObject.getValue().getProperties()) {
                    index++;
                    // Define a series
                    XYChart.Series<Number, Number> series = new XYChart.Series<>();
                    series.setName(prop);
                    this.dataSource.registerSeries(series, 10*index);
                    // Append the series to the chart
                    this.chart.getData().add(series);
                }
          });
        
    }
    
    @FXML
    private void handleEnableNameChanger() {
        this.nameChangerBtn.setDisable( !this.buttonEnablerChk.isSelected() );
    }
    
    @FXML
    private void handleChangeName() {
        this.nameChangerBtn.setText( this.newNameTxt.getText() );
    }
    
    @FXML
    private void handleRefreshingToggler() {
       if( this.dataSource.isRunning() ) {
           this.dataSource.stop();
           this.refreshButton.setText("Start Refreshing");

       } else {
           this.dataSource.start();
           this.refreshButton.setText("Stop Refreshing");
       }
    }

    public void handleExit() {
        this.dataSource.stop();
        System.exit(0);
    }
}