package benchmark_app;

import java.io.IOException;

import javax.security.auth.login.LoginException;

import benchmark_app.views.RootLayoutController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private RootLayoutController rootController;

    public MainApp() {
    }

    /**
     * Entry point: launches the app.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Entry point of the application. Takes a stage (the window) as input
     * @throws LoginException 
     */
    @Override
    public void start(Stage primaryStage) throws LoginException {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Benchmark Application");

        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("views/RootLayout.fxml"));
            this.rootLayout = (BorderPane) loader.load();
            this.rootController = loader.getController();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            primaryStage.show();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void stop() {
        this.rootController.handleExit();
    }

}
