# -*- coding: utf-8 -*-

import random, math, datetime
import pyjapc
from PyQt5 import QtCore, QtChart


class DataSource(QtCore.QObject):
    """
        This class performs the conn
    """
    def __init__(self, parent=None):
        super().__init__(parent)

        self._data = []
        self._update_series = True
        self._datasource_updates_count = 0
        self.start_time = datetime.datetime.now()

        self._japc = pyjapc.PyJapc( noSet = True )
        self._japc.rbacLogin()
        self._japc.subscribeParam( "LHC.BSRTS.5R4.B1/Image", self.onDataReceived , getHeader=True, unixtime = False)
        self._japc.startSubscriptions()

    updateSeriesChanged = QtCore.pyqtSignal(bool)

    @QtCore.pyqtProperty(bool, notify=updateSeriesChanged)
    def update_series(self):
        return self._update_series

    @update_series.setter
    def update_series(self, update):
        self._update_series = update

    """
        Signals can be empty, not carrying any value. These signals are meant to trigger some reaction on the GUI side,
        like a refresh.
    """
    dataReceived = QtCore.pyqtSignal()

    @QtCore.pyqtProperty(list, notify=dataReceived)
    def data(self):
        return self._data

    def onDataReceived(self, subscription_string, fields, header):
        self._datasource_updates_count += 1
        if self._update_series:
            self._data = fields["imageSet"].tolist()
            self.dataReceived.emit()

    @QtCore.pyqtProperty(float, notify=dataReceived)
    def updates_frequency(self):
        return self._datasource_updates_count / (datetime.datetime.now() - self.start_time).total_seconds()


class ListOfObjects(QtCore.QAbstractListModel):
    """
        List models are not trivial to implement in Qt. They cannot be simple Python/C++ classes, but they
        must extend an AbstractListModel class and reimplement some methods, namely .rowCount(), .data(),
        .roleNames(), in order to work properly with QML components.
        An interesting feature of AbstractModels in Qt are roles: a role must be defined for each property
        of the children elements that we want to expose to the holders of this series.
    """

    NameRole =  QtCore.Qt.UserRole + 1
    PropertiesRole =  QtCore.Qt.UserRole + 2
    QmlSeriesRole =  QtCore.Qt.UserRole + 3
    ChartNameRole =  QtCore.Qt.UserRole + 4

    def __init__(self, parent=None):
        super().__init__(parent)
        self._internal_list = [ComplexObject(suffix=number, props=number) for number in range(1, 5)]

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._internal_list)

    def roleNames(self):
        return {
            self.NameRole : "name".encode('utf-8'),
            self.PropertiesRole : "properties".encode('utf-8'),
            self.QmlSeriesRole : "qml_series".encode('utf-8'),
            self.ChartNameRole : "chart_name".encode('utf-8'),
        }

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if index.isValid():
            if role == QtCore.Qt.DisplayRole:
                return QtCore.QVariant(self._internal_list[index.row()])

            elif role == self.NameRole:
                return self._internal_list[index.row()].name
            elif role == self.PropertiesRole:
                return self._internal_list[index.row()].properties
            elif role == self.QmlSeriesRole:
                return self._internal_list[index.row()].qml_series
            elif role == self.ChartNameRole:
                return self._internal_list[index.row()].chart_name

        return QtCore.QVariant()

    @QtCore.pyqtSlot('QModelIndex', result='QVariant')
    def get(self, index):
        """
            Given that .data() is not a slot, it cannot be called directly from QML.
            The only way to access this method is by re-exposing it through a slot.
        """
        return self.data(index)


class ComplexObject(QtCore.QObject):
    """
        Wraps a few fields that will be used to render the plot and to identify the different objects in different views.
        Simulates a model.
    """

    def __init__(self, suffix="", props=1, name="Parent_Object_", parent=None):
        super().__init__(parent)

        self._name = "{}{}".format(name, suffix)
        self._properties = ["{}_property{}".format(self._name, number) for number in range(int(props))]

        self._qml_series = []
        # self._series_list = []
        self._chart_name = "Chart of {}".format(self._name)

    """
        Signals will be triggered when the relative properties are modified. They are linked through the `notify` 
        parameter of QtCore.pyqtSignal()
    """
    nameChanged = QtCore.pyqtSignal(str)

    @QtCore.pyqtProperty(str, notify=nameChanged)
    def name(self):
        """
            Properties should be exposed to QML by providing a getter method decorated with @QtCore.pyqtProperty.
            In this way, one can access the property from QML by simply typing `object.property`.
            The `notify` parameter points to the signal that should be triggered when this property is modified.
        """
        return self._name

    @name.setter
    def name(self, name):
        """
            For each property, if a setter exists and is decorated properly, in QML one can set the property by doing
            'object.property_with_a_setter = new_value'
        """
        self._name = name

    @QtCore.pyqtProperty(list)
    def properties(self):
        """
            Given that we don't plan to set the properties, the setter and the signal can be left out.
            However, the getter is still useful, as it provides access to the property through the parent list.
        """
        return self._properties

    @QtCore.pyqtProperty(str)
    def chart_name(self):
        return self._chart_name

    qmlSeriesChanged = QtCore.pyqtSignal(str)

    @QtCore.pyqtProperty(list, notify=qmlSeriesChanged)
    def qml_series(self):
        return self._qml_series

    @qml_series.setter
    def qml_series(self, series):
        self._qml_series = series

    @QtCore.pyqtProperty(int)
    def series_count(self):
        return len(self._qml_series)

    @QtCore.pyqtSlot(int, result="QVariant")
    def series(self, index):
        """
            A slot is a method which is directly callable from QML.
            The decorator defines the signature that will be exposed to QML, which is, in this case,
            one int parameter and a QVariant return object.
        """
        return self._qml_series[index]

    @QtCore.pyqtSlot(list, float)
    def update_qml_series(self, data, offset):
        """
            This slot moves part of the rendering logic back into Python.
            By passing back the reference of the generate LineSeries, they expose to Python methods
            that were not accessible from QML, namely the crucial .blockSignals() method. By leveraging
            these additional APIs, performances can be improved up to 20x (from a max of 1000 pt to a
            max of 20.000 pt smoothly rendered per each of the 4 series)
        """
        for s_index, series in enumerate(self._qml_series):
            series.blockSignals(True) # This call prevents the chart's repainting, speeding up significantly!
            series.clear()
            for p_index, point in enumerate(data):
                series.append(p_index, point + offset * s_index)
            series.blockSignals(False) # This call re-enables the signals, so the following signal can trigger the repaint
            series.pointsReplaced.emit()



