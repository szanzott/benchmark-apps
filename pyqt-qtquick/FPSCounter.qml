import QtQuick 2.0


Item {
    id: root
    z: 10000000;

    // *************** API ******************************
    property bool showCurrentFPS: true
    property bool showTenFoldAverageFPS: false
    property bool showRunningAverageFPS: true
    property bool showReset: true
    property bool showBanner: true
    property bool consoleOutput: false

    property alias fontSize: fpsLabel.font.pointSize
    property color backgroundColor: "blue";
    property color textColor: "yellow";
    property color highlightColor: "red";
    property color borderColor: "blue";
    // **************************************************

    readonly property double fpsRunningAvg: priv.fpsRunningAvg.toFixed(2);

    QtObject {
        id: priv
        property int frameCounter: 0
        property int frameCounterAvg: 0
        property int counter: 0
        property int fps: 0
        property double fpsAvg: 0
        property double fpsRunningAvg: 0
        property int fpsRunningAvgLoops: 0
    }


    // This is the trick that counts the frames
    Image {
        NumberAnimation on rotation {
            from: 0
            to: 360
            duration: 1000
            loops: Animation.Infinite
        }
        onRotationChanged: priv.frameCounter++; // counts the frames
    }

    Timer {
        interval: 200
        repeat: true
        running: true
        property double rate: interval/1000;

        onTriggered: {
            // FPS and Running Average update
            priv.fps = priv.frameCounter/rate;
            priv.fpsRunningAvgLoops++;
            priv.fpsRunningAvg += (priv.fps - priv.fpsRunningAvg) / priv.fpsRunningAvgLoops;

            // 10-fold averaged FPS update
            priv.frameCounterAvg += priv.frameCounter;
            priv.counter++; // counts the updates of the fast label
            if (priv.counter > 10) {
                priv.fpsAvg = priv.frameCounterAvg/(rate*priv.counter)
                priv.frameCounterAvg = 0;
                priv.counter = 0;

                if(consoleOutput){
                    console.log("    Current FPS: " + priv.fps + "\n" +
                                "10-fold avg FPS: " + priv.fpsAvg.toFixed(0) + "\n" +
                                "Running avg FPS: " + priv.fpsRunningAvg.toFixed(0) + "\n" );
                }
            }
            priv.frameCounter = 0;
        }
    }

    function printRecap(){
        console.log("[FPSCounter Summary] Average FPS: " + priv.fpsRunningAvg.toFixed(0) + "\n" );
    }



    Rectangle {
        id: visibleThings
        visible: showBanner

        color: backgroundColor
        border.color: borderColor;
        width:  childrenRect.width + 14;
        height: childrenRect.height + ( showReset? 7 : -7);

        Text {
            id: fpsLabel
            x: 5
            y: 5
            color: textColor
            font.bold: true
            font.pointSize: 8
            font.family: "Courier New"

            property string currentfps:    showCurrentFPS ?        "    Current FPS: " + priv.fps + "\n" : ""
            property string tenfoldfps:    showTenFoldAverageFPS ? "10-fold avg FPS: " + priv.fpsAvg.toFixed(0) + "\n" : ""
            property string runningavgfps: showRunningAverageFPS?  "Running avg FPS: " + priv.fpsRunningAvg.toFixed(0) + "\n" : ""

            text: currentfps + tenfoldfps + runningavgfps
        }

        Text {
            visible : showReset

            x: fpsLabel.x
            y: fpsLabel.height - fpsLabel.font.pointSize + 3
            color: mouseArea.containsMouse? highlightColor : textColor;
            font.bold: true
            font.pointSize: fpsLabel.font.pointSize;
            font.family: "Courier New"
            text: "       RESET       "
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                hoverEnabled: true
                onClicked: reset()
            }
        }
    }

    function reset(){
        priv.frameCounter =  0
        priv.frameCounterAvg = 0
        priv.counter = 0
        priv.fps = 0
        priv.fpsAvg = 0
        priv.fpsRunningAvg = 0
        priv.fpsRunningAvgLoops = 0
    }
}


