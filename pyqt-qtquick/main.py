# -*- coding: utf-8 -*-

import sys
from custom_model import ListOfObjects, DataSource

from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine, QQmlComponent
from PyQt5.QtCore import QUrl


if __name__ == '__main__':
    
    # Create the application instance
    app = QApplication(sys.argv)

    # Create a QML engine
    engine = QQmlApplicationEngine()
    
    # Instantiate the model
    list_of_objects = ListOfObjects()
    data_source = DataSource()

    # Register the model in the context
    engine.rootContext().setContextProperty('listOfObjects', list_of_objects)
    engine.rootContext().setContextProperty('dataSource', data_source)

    # Create a component factory and load the QML script
    component = QQmlComponent(engine)
    component.loadUrl(QUrl('main.qml'))
    
    # Create an instance of the component
    qmlInstance = component.create()

    # Enter the event loop
    sys.exit(app.exec_())   




