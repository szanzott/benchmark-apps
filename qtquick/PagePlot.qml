import QtQuick 2.9
import QtQuick.Controls 2.4
import QtCharts 2.0

Item {
    id: plotTab
    property var listOfObjects;

    Component.onCompleted: {
        // chartView.model = listOfObjects.get( comboBox1.currentIndex )
        chartView.model = listOfObjects[comboBox1.currentIndex];
        chartView.generateSeries()
    }

    Connections {
        target: dataSource
        onDataReceived: chartView.refreshSeries()
    }

    Label {
        id: labelDesc1
        text: qsTr("These two ComboBoxes are related one with the other. They are populated dynamically and populate the chart too.")
        anchors.top: parent.top
        anchors.topMargin: 13
        font.pointSize: 9
        font.italic: true
        wrapMode: Text.WordWrap

        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.right: parent.right
        anchors.rightMargin: 13
    }

    ComboBox {
        id: comboBox1

        wheelEnabled: false
        model: listOfObjectsQML
        textRole: 'name'
        currentIndex: 0
        onCurrentIndexChanged: {
            chartView.model = listOfObjects[comboBox1.currentIndex]
            chartView.generateSeries()
        }
        Component.onCompleted: currentIndex = 3

        anchors.right: parent.horizontalCenter
        anchors.rightMargin: 33
        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.top: labelDesc1.bottom
        anchors.topMargin: 13
    }

    ComboBox {
        id: comboBox2
        // model: listOfObjects.get(comboBox1.currentIndex).properties
        model: chartView.model.properties

        anchors.left: parent.horizontalCenter
        anchors.leftMargin: -27
        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.top: labelDesc1.bottom
        anchors.topMargin: 13
    }

    ChartView {
        id: chartView
        // property var model: listOfObjects.get(comboBox1.currentIndex).properties
        property var model: listOfObjects[comboBox1.currentIndex]

        title: listOfObjects[comboBox1.currentIndex].name
        antialiasing: true
        legend{
            alignment: Qt.AlignBottom
        }

        ValueAxis {
            id: xAxis
            min: 0
            max: slider.value
        }

        ValueAxis {
            id: yAxis
            min: 0
            max: 1000;
        }

        function maxArray(a) {
            var maxX = a[0];
            for (var x=0; x<a.length; x++)
                if (maxX < a[x])
                    maxX = a[x];
            return maxX;
        }

        function generateSeries() {
            // // console.log("----->", JSON.stringify( listOfObjects[0]) );
            removeAllSeries();
            // var seriesList = [];
            for(var s=0; s<chartView.model.properties.length; s++){
                var series = createSeries(ChartView.SeriesTypeLine, "Property_"+s, xAxis, yAxis)
                series.useOpenGL = true;
                // seriesList.push( series );
            }
            // chartView.model.qml_series = seriesList;
            refreshSeries();
        }

        function refreshSeries() {
            var seriesData = dataSource.dataset.slice(0, slider.value);
            var numOfSeries = chartView.count
            var offset = maxArray(seriesData)/10
            yAxis.max = offset * (10 + numOfSeries + 1);

            // console.log(offset, maxArray(seriesData), slider.value)

            for(var s=0; s<numOfSeries; s++){
                chartView.series(s).clear()
                for(var p = 0; p < seriesData.length; p++){
                    chartView.series(s).append(p, seriesData[p] + offset*s)
                }
            }
        }

        anchors.bottom: sliderLabel.top
        anchors.bottomMargin: -20
        anchors.top: comboBox1.bottom
        anchors.topMargin: 7
        anchors.right: parent.right
        anchors.rightMargin: 7
        anchors.left: parent.left
        anchors.leftMargin: 7
    }

    Label {
        id: sliderLabel
        text: "Data Source Statistics:
Batch Size:\t\t" + dataSource.dataset.length + " points.
Updates Frequency:\t" + dataSource.updates_frequency.toFixed(2) + " Hz"
        font.pointSize: 9
        wrapMode: Text.WordWrap

        anchors.bottom: sliderLabel2.top
        anchors.bottomMargin: 7
        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 13
    }

    Label {
        id: sliderLabel2
        text: "Note: as a benchmark for the chart, you can use the slider below to increase the amount of data processed (from 1 to batchsize)"
        font.pointSize: 9
        font.italic: true
        wrapMode: Text.WordWrap

        anchors.bottom: slider.top
        anchors.bottomMargin: 7
        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 13
    }


    Slider {
        id: slider
        from: 0
        to: dataSource.dataset.length
        value: 100
        stepSize: 1
        onValueChanged: spinBox.value = value

        anchors.bottom: buttonRefresh.top
        anchors.bottomMargin: 7
        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 13

        Component.onCompleted: {
            // console.log(dataSource.dataset.length);
        }
    }

    Button {
        id: buttonRefresh
        text: qsTr("Stop Refreshing")
        onClicked: {
            if(buttonRefresh.text == "Stop Refreshing"){
                buttonRefresh.text = "Start Refreshing";
            } else {
                buttonRefresh.text == "Stop Refreshing";
            }
            dataSource.update_series = !dataSource.update_series
        }

        anchors.bottom: parent.bottom
        anchors.bottomMargin: 24
        anchors.right: parent.right
        anchors.rightMargin: 24
    }

    SpinBox {
        id: spinBox
        from: slider.from
        to: slider.to
        stepSize: 100
        value: slider.value
        onValueChanged: slider.value = value

        anchors.bottom: parent.bottom
        anchors.bottomMargin: 24
        anchors.left: parent.left
        anchors.leftMargin: 24
        anchors.right: buttonRefresh.left
        anchors.rightMargin: 24
    }
}
