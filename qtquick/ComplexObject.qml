import QtQuick 2.0

Item {

    property string name: "Unnamed_Parent_Object"
    property int number_of_properties: 0
    property string chart_name: "Chart of " + name
    property var properties: []

    /*
    onNumber_of_propertiesChanged: {
        properties = []
        for(var p=0; p<number_of_properties; p++){
            properties.push( name+"_property_"+p )
        }
        console.log("### ", properties)
    }
    */
}
