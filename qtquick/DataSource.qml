import QtQuick 2.0
import Qt.WebSockets 1.0
import RDA3 1.0

Item {

    property var dataset: []
    property bool update_series: true
    property double updates_frequency: 0
    property var start_time: new Date()

    signal dataReceived;

    function connect(){
        // rdaDevicePropertyAccess.subscribe("LHC.BSRTS.5R4.B1/Image#imageSet", "", "")
        console.log("HI!")
        rdaDevicePropertyAccess.subscribe("PR.SCOPE96.CONCENTRATED/Acquisition#MAG_CYCLE", "", "cpsFixdisplayConcentratorDevS")
    }

    function disconnect(){
        rdaDevicePropertyAccess.unsubscribe("LHC.BSRTS.5R4.B1/Image#imageSet", "", "")
    }

    Rda3QtDPA {
        id: rdaDevicePropertyAccess
        property int datasource_updates_count: 0

        onRdaDataAcquiredChanged: {
            console.log("Hello!")
            dataset = rdaDevicePropertyAccess.rdaDataAcquired["LHC.BSRTS.5R4.B1/Image#imageSet"];
            datasource_updates_count++;
            updates_frequency = datasource_updates_count / (new Date() - start_time).toFixed(0)
            dataReceived() // This call triggers the parent's signal
        }
    }
}
