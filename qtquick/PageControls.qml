import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

Item {
    id: controls

    property var listOfObjects;

    CheckBox {
        id: checkBox
        text: qsTr("Enable This Button")

        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.top: parent.top
        anchors.topMargin: 13
    }

    Button {
        id: buttonMyName
        text: "Change My Name"
        enabled: checkBox.checked

        anchors.left: checkBox.right
        anchors.leftMargin: 14
        anchors.top: parent.top
        anchors.topMargin: 13

        onClicked: text = textField.text
    }

    TextField {
        id: textField

        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.left: buttonMyName.right
        anchors.leftMargin: 13
        anchors.top: parent.top
        anchors.topMargin: 13
    }



    ToolSeparator {
        id: toolSeparator
        orientation: Qt.Horizontal

        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: textField.bottom
        anchors.topMargin: 13
    }


    Label {
        id: labelList
        text: "List (contains the same data as the combobox)"

        font.italic: true;
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap

        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.top: toolSeparator.bottom
        anchors.topMargin: 13
    }

    Rectangle {
        id: scrollViewBorder
        border.color: "grey"

        anchors.right: parent.right
        anchors.rightMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.top: labelList.bottom
        anchors.topMargin: 13
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 13

        ScrollView {
            id: scrollView
            anchors.fill: parent
            anchors.margins: 2

            ListView {
                id: listView
                anchors.fill: parent
                model: listOfObjectsQML
                delegate: ListDelegate {}
            }
        }
    }

    Popup {
        id: popup

        x: Math.max( (window.width-200)/2, 0)
        y: Math.max( (window.height-200)/2, 0)
        width: 200
        height: 200

        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        contentItem:  Label {
            height: 40
            color: "#2b2826"
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            fontSizeMode: Text.HorizontalFit
            wrapMode: Text.WordWrap
            text: "No operation selected :("
        }

        function displayAction(actionName){
            contentItem.text = "You just tried to "+actionName+" word!"
            open()
        }
    }
}
