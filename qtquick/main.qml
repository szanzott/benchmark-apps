import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11


Window {
    id: window
    visible: true
    width: 480
    height: 640
    title: qsTr("Benchmark Application")

    property var listOfObj: [];

    ListModel {
        id: listOfObjectsQML
    }

    DataSource {
        id: dataSource
    }

    Component.onCompleted: {

        dataSource.connect()

        for(var o=1; o<=4; o++){
            var complexObject = Qt.createComponent("ComplexObject.qml")
            complexObject.name = "Parent_Object_"+(o)
            var properties = []
            for(var p=0; p<o; p++){
                properties.push( complexObject.name+"_property_"+p );
            }
            complexObject.properties = properties;
            //listOfObjects.append( complexObject )
            listOfObj.push( complexObject )
            listOfObjectsQML.append(complexObject);
        }

        // console.log("###", JSON.stringify(listOfObj[1].properties) )
    }

    Label {
        id: labelTitle
        color: "#0057c4"
        text: qsTr("Reference Interface")
        font.capitalization: Font.SmallCaps
        font.underline: true
        font.italic: true
        font.bold: true
        font.pointSize: 16
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap

        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.top: parent.top
        anchors.topMargin: 13
        anchors.right: parent.right
        anchors.rightMargin: 13
    }

    TabBar {
        id: tabBar

        anchors.top: labelTitle.bottom
        anchors.topMargin: 13
        anchors.left: parent.left
        anchors.leftMargin: 13
        anchors.right: parent.right
        anchors.rightMargin: 13

        TabButton {
            id: tabBtnPlot
            text: qsTr("Plot")
        }

        TabButton {
            id: tabBtnControls
            text: qsTr("Controls")
        }
    }

    StackLayout {
        anchors.top: tabBar.bottom
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        currentIndex: tabBar.currentIndex

        PagePlot {
            listOfObjects: listOfObj;
        }

        PageControls {
            listOfObjects: listOfObj;
        }
    }
}
