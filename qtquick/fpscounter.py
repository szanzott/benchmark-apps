import matplotlib.pyplot as plt
import numpy as np
import os.path

datalist = []
i = 2
while os.path.isfile('fpsvalues/fpsvalues-{}'.format(i)):
     data = np.genfromtxt('fpsvalues/fpsvalues-{}'.format(i), delimiter=":", skip_header=8)
     print(data.shape, i)
     datalist.append( data[:, 1:] )
     i += 2

print(datalist[0].shape)

min_x = min( [data.shape[0] for data in datalist] )
x = datalist[0][:min_x, 0]

y_stack = np.column_stack( ( data[:min_x, 1] for data in datalist ) )
y = np.mean(y_stack, axis=1)
y_min = np.min(y_stack, axis=1)
y_max = np.max(y_stack, axis=1)

fig = plt.figure()

ax1 = fig.add_subplot(111)

ax1.set_title("FPS Values vs Number of Points")
ax1.set_xlabel('Number of Points')
ax1.set_ylabel('FPS Value')

plt.xticks(np.arange(0, max(x)+500, 500))
plt.yticks(np.arange(0, 60, 2))
#plt.rc('grid', linestyle="dashed")
plt.minorticks_on()
plt.grid(which="major", color='#CCCCCC', linestyle='solid', linewidth=1)
plt.grid(which="minor", color='#DDDDDD', linestyle='dashed', linewidth=1)

plt.ylim(0, 60)
plt.autoscale(False)

ax1.plot(x,y, c='r')
ax1.plot(x,y_min, c='#FFAAAA')
ax1.plot(x,y_max, c='#FFAAAA')
ax1.fill_between(x, y_min, y_max, facecolor="#FFEEEE")

for data in datalist:
     ax1.scatter(data[:min_x, 0], data[:min_x, 1], c='#FFAA00', marker='o', s=1)


leg = ax1.legend()

plt.show();