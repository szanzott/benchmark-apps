import QtQuick 2.9
import QtQuick.Controls 2.4


Rectangle {
    width: parent.width
    height: 30
    color: index % 2 == 0 ? "white" : "lightgrey"
    anchors.horizontalCenter: parent.horizontalCenter


    Label {
        text: model.name
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        font.italic: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: {
                if (mouse.button === Qt.leftButton || mouse.button === Qt.rightButton)
                    contextMenuQML.popup()
            }
            onPressAndHold: {
                if (mouse.source === Qt.MouseEventNotSynthesized)
                    contextMenuQML.popup()
            }

            Menu {
                id: contextMenuQML
                Action { text: "Cut"; onTriggered: popup.displayAction("cut a") }
                Action { text: "Copy"; onTriggered: popup.displayAction("copy a") }
                Action { text: "Paste"; onTriggered: popup.displayAction("paste a") }

                MenuSeparator { }

                Menu {
                    title: "Find/Replace"
                    Action { text: "Find Next"; onTriggered: popup.displayAction("find next") }
                    Action { text: "Find Previous"; onTriggered: popup.displayAction("find the previous") }
                    Action { text: "Replace"; onTriggered: popup.displayAction("replace a") }
                }
            }
        }
    }
}
