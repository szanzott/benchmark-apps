#include "complexobject.h"

ComplexObject::ComplexObject(QObject *_parent, QString name, int num_props) : QObject(_parent)
{
    m_name = name;
    m_chartName = "Chart of " + name;
    for(int i=0; i<num_props; i++){
        m_properties << name + QString("_property_%1").arg(i);
    }
}

QString ComplexObject::name() const
{
    return m_name;
}

QString ComplexObject::chartName() const
{
    return m_chartName;
}

QList<QString> ComplexObject::properties() const
{
    return m_properties;
}
