#include <iostream>
#include <QtCore/QSize>
#include "listofobjects.h"

ListOfObjects::ListOfObjects(QObject *_parent) : QAbstractListModel(_parent)
{
    m_roleNames.insert( NAME_ROLE, QByteArray("name") );
    m_roleNames.insert( CHART_NAME_ROLE, QByteArray("chartName") );
    m_roleNames.insert( PROPERTIES_ROLE, QByteArray("properties") );

    for(int i=1; i<5; i++){
        m_list << new ComplexObject(this->parent(), QString("Parent_Object_%1").arg(i), i);
    }
}

int ListOfObjects::rowCount(const QModelIndex &parent) const
{
    return m_list.length();
}

QHash<int, QByteArray> ListOfObjects::roleNames() const
{
    return m_roleNames;
}

QVariant ListOfObjects::data(const QModelIndex &index, int role) const
{
    if(index.isValid()){
        if(role == Qt::DisplayRole){
            return m_list.at(index.row())->name();
        }
        else if (role == Qt::TextAlignmentRole){
            return Qt::AlignCenter;
        }
        else if (role == Qt::SizeHintRole) {
            return QSize(100, 25);
        }
        else if(role == NAME_ROLE){
            return m_list.at(index.row())->name();
        }
        else if(role == CHART_NAME_ROLE){
            return m_list.at(index.row())->chartName();
        }
        else if(role == PROPERTIES_ROLE){
            return QVariant(m_list.at(index.row())->properties());
        }
    }
    return QVariant();
}
