#ifndef LISTOFOBJECTS_H
#define LISTOFOBJECTS_H

#include <QtCore/QObject>
#include <QtCore/QModelIndex>
#include "complexobject.h"

class ListOfObjects: public QAbstractListModel {
    Q_OBJECT

private:
    QList<ComplexObject*> m_list{};
    QHash<int, QByteArray> m_roleNames{};

public:
    const int NAME_ROLE = Qt::UserRole + 1;
    const int PROPERTIES_ROLE = Qt::UserRole + 2;
    const int CHART_NAME_ROLE = Qt::UserRole + 4;

    explicit ListOfObjects(QObject *_parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

};

#endif // LISTOFOBJECTS_H
