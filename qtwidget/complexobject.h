#ifndef COMPLEXOBJECT_H
#define COMPLEXOBJECT_H

#include <QtCore/QObject>

class ComplexObject: public QObject {
    Q_OBJECT
    Q_PROPERTY(QString name MEMBER m_name)
    Q_PROPERTY(QString chartName MEMBER m_chartName)
    Q_PROPERTY(QList<QString> properties MEMBER m_properties)

private:
    QString m_name;
    QString m_chartName;
    QList<QString> m_properties;

public:
    ComplexObject(QObject *_parent = nullptr, QString name = "Unnamed_Parent_Object_", int num_props = 1);

    QString name() const;
    QString chartName() const;
    QList<QString> properties() const;
};

#endif // COMPLEXOBJECT_H
