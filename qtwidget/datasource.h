#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <time.h>
#include <QtCore/QObject>

class DataSource: public QObject {
    Q_OBJECT
    Q_PROPERTY(int* data MEMBER m_data NOTIFY dataChanged)
    Q_PROPERTY(float updates_frequency READ updatesFrequency)
    Q_PROPERTY(bool update_series MEMBER m_update_series NOTIFY updateSeriesChanged)

private:
    int* m_data;
    bool m_update_series;
    int m_datasource_updates_count;
    time_t m_start_time;

    float updatesFrequency() const;

public:
    explicit DataSource(QObject *_parent = nullptr);

    int* data();
    void dataReceived(int* newData);

signals:
    void dataChanged(int* newData);
    void updateSeriesChanged(bool newUpdateSeries);
};

#endif // DATASOURCE_H
