#include <iostream>

#include <QtCore/QStringList>
#include <QtCore/QStringListModel>
//#include <QtCharts/QChart>
//#include <QtCharts/QValueAxis>
#include <QtCharts>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Wiring the model <------------- FIX THIS
    connect(
        &this->m_dataSource, &DataSource::dataChanged,
        [&](int* data){
            auto str_data = std::to_string(*data);
            ui->lineEdit->setText(QString::fromStdString(str_data));
        }
    );

    // // Setup the chart
    QChart* chart = new QChart();
    chart->setBackgroundBrush(QBrush(Qt::white));

    // Why this causes a segfault???
    // connect(
    //     ui->comboBox_1, &QComboBox::currentTextChanged,
    //     [&](){ chart->setTitle("Chart of "+ui->comboBox_1->currentText()); }
    // );
    //chart->setTitle("Chart of "+ui->comboBox_1->currentText());

    QValueAxis* xAxis = new QValueAxis();
    chart->setAxisX(xAxis);
    QValueAxis* yAxis = new QValueAxis();
    chart->setAxisY(yAxis);
    connect( ui->horizontalSlider, &QSlider::valueChanged, xAxis, &QValueAxis::setMax );

    ui->chart_view->setChart(chart);


    // ComboBoxes setup
    ui->comboBox_1->setModel(&m_listOfObjects);
    ui->comboBox_2->setModel(&m_listOfObjectSecondComboboxModel);

    connect(
        ui->comboBox_1, &QComboBox::currentTextChanged,
        [&](){
            auto index = ui->comboBox_1->currentIndex();
            auto modelIndex = m_listOfObjects.index(index, 0);
            QStringList list = m_listOfObjects.data(modelIndex, m_listOfObjects.PROPERTIES_ROLE).toStringList();
            m_listOfObjectSecondComboboxModel.setStringList(list);
            ui->comboBox_2->setCurrentIndex(0);
    });
    ui->comboBox_1->setCurrentIndex(0);

    connect(ui->refreshButton, &QPushButton::clicked, this, &MainWindow::toggleChartRefresh);
    // Start/Stop refreshing
    // self.refreshButton.clicked.connect(self.toggle_chart_refresh)


    // Name-changing button
    connect(
        ui->pushButton_2, &QPushButton::clicked,
        [&](){ui->pushButton_2->setText(ui->lineEdit->text());}
    );

    // Setup listview
    ui->listView->setModel(&m_listOfObjects);


    // **************** EXAMPLES ************************
    // Two connected jumps, triggered by the button
    /*
    connect(
            &this->m_dataSource, &DataSource::dataChanged,
            [&](int* data){
                auto str_data = std::to_string(*data);
                ui->lineEdit->setText(QString::fromStdString(str_data));
            }
    );

    connect(
                ui->pushButton_2, &QPushButton::clicked,
                //[&](){ui->lineEdit->setText("Ciao, Sarina");}
                [&](){m_dataSource.dataReceived(&A); A++;}
    );
    */
}

void MainWindow::toggleChartRefresh(){
    std::cout << "HELLO" << std::endl;
}

void MainWindow::generateSeries(){

}

void MainWindow::updateSeries(){

}

MainWindow::~MainWindow()
{
    delete ui;
}
