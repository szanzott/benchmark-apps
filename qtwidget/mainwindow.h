#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QStringListModel>
#include "datasource.h"
#include "listofobjects.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    DataSource m_dataSource{this};
    ListOfObjects m_listOfObjects;
    QStringListModel m_listOfObjectSecondComboboxModel;

    void toggleChartRefresh();

public slots:
    void updateSeries();
    void generateSeries();

};

#endif // MAINWINDOW_H
