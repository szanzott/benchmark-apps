#include "datasource.h"

DataSource::DataSource(QObject *_parent) : QObject(_parent) {
    time(&this->m_start_time);
    m_data = new int[10]{5,3,6,2,8,9,0,1,7,6};
    m_update_series = true;
    m_datasource_updates_count = 0;
}

void DataSource::dataReceived(int* newData) {
    if(m_data != newData) {
        m_data = newData;
        emit dataChanged(m_data);
    }
}

int* DataSource::data(){
    return m_data;
}

float DataSource::updatesFrequency() const {
    return 0.0;
}
